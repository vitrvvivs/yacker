yacker
======

.. highlight:: none

*  Love `Packer`_?
*  Frustrated by the lack of support for `YAML`_?
*  Even though the process of converting YAML to JSON is, well, simple?

Well, now there's `yacker`_!

``yacker`` is an open-source Python YAML interpreter for HashiCorp's Packer.
It wraps Packer in a converting wrapper that allows you to drive
it with YAML files.

Now you can craft your Packer files in YAML rather than JSON. Then,
instead of running ``packer`` on JSON files, you run ``yacker`` on your
YAML files::

   yacker build template.yml

All ``yacker`` does is translate YAML to JSON *on the fly* (a super-trivial
act in Python), calling on Packer to do the rest.

Now you can benefit from the more pleasant environment that YAML provides
and still use a pretty cool tool.

``yacker``: -

*  searches the command-line for ``.yml`` (or ``.yaml``) files (specifically
   looking for the template and any ``-var-file`` references)
*  converts the files (if found) to JSON files
*  reconstructs the command line (using the remaining options)
*  passes control to ``packer``
*  removes the JSON files it created
*  passes control (and Packer's exit code) back to you

Packer's output and any errors are simply presented to you as if you'd
run Packer using JSON.

Variable Files
--------------

``yacker`` also translates any YAML-based variable files you may have.
If you name a YAML file in any ``-var-file`` argument ``yacker`` will
temporarily convert that file for you before passing it to Packer and
then removing it::

    yacker build -var-file=v1.yml -var-file=v2.yml template.yml

YACKER_PACKER_PATH
------------------

``yacker`` expects to find ``packer`` on the system ``PATH``.

If you don't have Packer on the system path, or want to use a different Packer,
you can provide a path to it by using the ``YACKER_PACKER_PATH`` environment
variable::

    YACKER_PACKER_PATH=~/bin yacker version

Or::

    export YACKER_PACKER_PATH=~/bin
    yacker version
    yacker build template.yml

.. _Packer: https://www.packer.io
.. _Yacker: https://pypi.org/project/matildapeak-yacker/
.. _YAML: http://yaml.org
